import { LitElement, html } from "../node_modules/lit-element/lit-element.js";
export class ComponentMovimiento extends LitElement {
  static get properties() {
    return {
      tipoMovimiento: {
        type: String
      },
      cantidad: {
        type: String
      },
      fecha: {
        type: String
      },
      movimiento: {
        type: Object
      },
      dividendo: {
        type: Number
      }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <div class="container">
        <div class="row">
          <div class="col">
            <img src="https://as2.ftcdn.net/jpg/00/72/77/79/500_F_72777900_PuZflEq56bzqNr7SqGq2X59MsfC9aDPp.jpg" alt="" width="30px" height="30px">
          </div>
          <div class="col">
            <h3>${this.movimiento.tipoMovimiento}</h3>
          </div>
          <div class="col-5" >
            <h3>${(this.movimiento.cantidad / this.dividendo).toFixed(2)} ${this.dividendo == 1 ? "MXN" : "USD"}</h3>
          </div>
          <div class="col">
            <h3>${this.movimiento.fecha}</h3>
          </div>
        </div>
      </div>

    `;
  }

}
customElements.define('component-movimiento', ComponentMovimiento);