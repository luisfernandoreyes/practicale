import { LitElement, html } from 'lit-element';

export class ComponentLogin extends LitElement {

  static get properties(){
    return {
      emailUser: {type: String},
      passUser: {type: String},
      numeroCuenta: {type: String},
      logeado: {type: Boolean},
      isLogin: {type: Boolean}
    };
  }

  constructor(){
    super();
    this.isLogin = true;
  }

  render() {
    return html` <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <div class="container">

        ${this.isLogin ? html `` :
          html`<div class="form-group">
                <label for="exampleInputNcuenta">Numero de Cuenta</label>
                <div class="input-group mb-3">
                  <input type="text" class="form-control" id="exampleInputNcuenta" placeholder="07234512" aria-label="Recipient's username" aria-describedby="basic-addon3" @change = "${this.updateNumeroCuenta}" ></input>
                  <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon3">
                      <img src="https://img.icons8.com/ultraviolet/40/000000/bank-cards.png" alt="" width="20px" height="20px"/>
                    </span>
                  </div>
                </div>
              </div>`}
        <div class="form-group">
          <label for="exampleInputEmail1">Correo Electrónico</label>
          <div class="input-group mb-3">
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="ejemplo@gmail.com" aria-label="Recipient's username" aria-describedby="basic-addon2" @change = "${this.updateEmail}">
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2">
                <img src="https://img.icons8.com/nolan/64/email.png" alt="" width="20px" height="20px">
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1"  @change = "${this.updatePassword}">
        </div>
        <div class="row">
          ${this.isLogin ?
            html `<button type="button" class="btn btn-primary btn-lg btn-block" @click ="${this.logear}">Iniciar Sesión</button>
                  <button class="btn btn-light btn-lg btn-block" @click="${this.cambiarARegistro}">Registrate</button>` :
            html `<button type="button" class="btn btn-primary btn-lg btn-block"  @click="${this.registrar}">Registrar</button>
                  <button class="btn btn-light btn-lg btn-block" @click="${this.cambiarARegistro}">Inicia Sesión</button>`
          }

        </div>
  </div>
    `;
  }

  updateEmail(e){
    this.emailUser = e.target.value;
  }

  updatePassword(e){
    this.passUser = e.target.value;
  }

  updateNumeroCuenta(e){
    this.numeroCuenta = e.target.value;
  }

  cambiarARegistro(e){
    this.isLogin = !this.isLogin;
  }

  logear(e){
      var url = 'https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/Login';
      var data = {email: this.emailUser, password: this.passUser};
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => alert("email o password incorrecto"))
      .then(response => {
        if(response.usuario.verificado){
          this.logeado = !this.logeado;
          this.dispatchEvent(new CustomEvent('change',{
            detail: {logeado: this.logeado, token: response.token, numeroCuenta: response.usuario.numeroCuenta}
          }));
        }else{
            alert("Aun no ha verificado su cuenta");
        }
      });
  }

  enviarCorreoVerificacion(email){
    fetch("https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/send/"+email)
        .then(function(response) {
          return response.json();
        })
        .then(function(res){
          console.log(res);
        });
  }

  registrar(e){
      var url = 'https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/Registro';
      var data = {numeroCuenta: this.numeroCuenta, email: this.emailUser, password: this.passUser, verificado: false};
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        this.isLogin = !this.isLogin;
        this.enviarCorreoVerificacion(response.email);
        alert("Verifique su cuenta")
      });
  }
}



customElements.define('component-login', ComponentLogin);
