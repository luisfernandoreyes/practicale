import { LitElement, html } from 'lit-element';
import './component-login.js';
import './component-movimiento.js'

export class ComponentPrincipal extends LitElement {
  static get properties(){
    return {
      token: {type: String},
      logeado: {type: Boolean},
      cliente: {type: Object},
      movimientos: {type: Array},
      error: {type: String},
      numeroCuenta: {type: String},
      vistaDeAgregar: {type: Number},
      numeroCuentaDestino: {type: Number},
      tipoMovimiento: {type: String},
      cantidad: {type: Number},
      fecha: {type: String},
      valorDolar: {type: String},
      fechaDolar: {type: String},
      numeroAleatorio: {type: String},
      mensajeVerificado: {type: String},
      dividendo: {type: Number},
      moneda: {type: String},
      saldo: {type: Number},
      nombre: {type: String}
    };
  }

  constructor(){
    super();
    this.logeado = this.hayToken();
    this.movimientos = [];
    this.error = "";
    this.vistaDeAgregar = false;
    this.dividendo =1;
    this.moneda = "Dolares";
  }

  hayToken(){
    if(localStorage.getItem('token')) {
      this.token = localStorage.getItem('token')
      this.numeroCuenta = localStorage.getItem('numeroCuenta')
      this.nombre = localStorage.getItem("nombre")
      this.saldo = localStorage.getItem("saldo")
      this.getMovimientos();
      return true;
    }
    return false;
  }

  render() {
    return html`<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <div class="row align-items-center" style="background-color: #1565c0; color: white;">
          <div class="d-flex justify-content-start">
            <div class="col">
              <h2> BBVA MX </h2>
            </div>
          </div>
          ${this.logeado?
            html `<div class"d-flex flex-row-reverse">
                    <button type="button" class="btn btn-primary"  @click="${this.mostrarVistaAgregar}">Agregar Movimiento</button>
                    <button type="button" class="btn btn-info"  @click="${this.convertirDivisa}">Ver catidad en ${this.moneda}</button>
                    <button type="button" class="btn btn-light" @click="${this.cerrarSesion}"><h10>Cerrar Sesión</h10></button>
                  </div>` :
            html ``}
        </div>
      ${this.logeado?
      html `<div class="d-flex justify-content-center">
              <div class="col-2">
                <div class="card" style="width: 18rem; margin: 10px">
                  <img src="https://img.icons8.com/fluent/144/000000/user-male-circle.png" class="card-img-top" alt="...">
                  <div class="card-body" >
                    <h3>${this.nombre}</h3>
                    <h5>Saldo Actual: ${this.saldo} Pesos</h5>
                    </div>
                  </div>
              </div>
              <div class="col">
              ${!this.vistaDeAgregar? html `
                <div class="row justify-content-md-center">
                  <h3> Movimientos </h3>
                </div>
                <div class="row">
                  ${this.movimientos.length > 0 ?
                    html `<div class="col">
                            ${this.movimientos.map(item => html `<div class="row justify-content-md-center "><component-movimiento dividendo="${this.dividendo}" movimiento= "${JSON.stringify(item)}"></component-movimiento></div>`)}
                          </div> ` :
                    html `<div class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                          </div>`}
                </div>`:
                html `
                    <div class="row justify-content-center">
                      <div class="col-6">
                      <label for="agregarNumeroCuentaDestino">Numero de Cuenta Destino</label>
                      <input type="text" class="form-control" id="agregarNumeroCuentaDestino" placeholder="07234512"  @change = "${this.updateNumeroCuentaDestino}" ></input>
                      </div>
                    </div>
                    <div class="row justify-content-center">
                      <div class="col-6">
                      <label for="agregarTipoMovimiento">Tipo de Movimiento</label>
                      <input type="text" class="form-control" id="tipoMovimiento" placeholder="cargo/abono"  @change = "${this.updateTipoMovimiento}" ></input>
                      </div>
                    </div>
                    <div class="row justify-content-center">
                      <div class="col-6">
                      <label for="agregarCantidad">Cantidad</label>
                      <input type="text" class="form-control" id="agregarCantidad" placeholder="200"  @change = "${this.updateCantidad}" ></input>
                      </div>
                    </div>
                    <div class="row justify-content-center">
                      <div class="col-6">
                      <label for="agregarFecha">Fecha de operación</label>
                      <input type="date" class="form-control" id="agregarFecha" placeholder="13/07/2020"  @change = "${this.updateFecha}" ></input>
                      </div>
                    </div>
                    <div class="row justify-content-center" style="margin-top: 2px">
                      <div class="col-6">
                        <button type="button" class="btn btn-primary"  @click="${this.agregarMovimiento}">Agregar Movimiento</button>
                        <button type="button" class="btn btn-dark"  @click="${this.cancelarMovimiento}">Cancelar</button>
                      </div>
                    </div>
                  `}
              </div>
            </div>
            ` :
      html` ${this.getNumeroAleatorio()?
        html `<div class="container">
                  ${!this.mensajeVerificado ?
                  html `<div class="row justify-content-md-center"><button type="button" class="btn btn-primary"  @click="${this.verificar}">Verificar Cuenta</button></div>` :
                  html `<div class="row align-items-start"><h2>${this.mensajeVerificado}</h2></div> <div class="row align-items-center"><div class="col align-self-center"><a href="http://127.0.0.1:8081/components/practicale/">Ir a Iniciar Sesión</a></div></div>`}
              </div>` :
        html `<div class="container">
                <div class="row justify-content-md-center">
                  <component-login  @change="${this.doChange}"></component-login>
                </div>
              </div>`}`}`;
  }

  verificar(){
    fetch("https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/verify?id="+this.numeroAleatorio)
        .then(respuesta => respuesta.json())
        .then(res => {
          this.numeroAleatorio = false;
          this.mensajeVerificado = res.mensaje;
        });
  }

  getNumeroAleatorio(){
    var sPaginaURL = window.location.search.substring(1);
    var sURLVariables = sPaginaURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var sParametro = sURLVariables[i].split('=');
      if (sParametro[0] == "id") {
        this.numeroAleatorio = sParametro[1];
        return true
      }
    }
    return false
  }

  updateNumeroCuentaDestino(e){
    this.numeroCuentaDestino = e.target.value;
  }

  updateTipoMovimiento(e){
    this.tipoMovimiento = e.target.value;
  }

  updateCantidad(e){
    this.cantidad = e.target.value;
  }

  updateFecha(e){
    this.fecha = e.target.value;
  }

  mostrarVistaAgregar(e){
    this.vistaDeAgregar = !this.vistaDeAgregar;
  }

  doChange(e){
    this.logeado = e.detail.logeado;
    this.token = e.detail.token;
    this.numeroCuenta = e.detail.numeroCuenta;
    localStorage.setItem('token', this.token);
    localStorage.setItem('numeroCuenta', this.numeroCuenta);
    this.getMovimientos();
  }

  getMovimientos(){
    var url = 'https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/Movimientos/'+this.numeroCuenta;
    fetch(url, {
      method: 'GET',
      headers:{
        'access-token' : this.token
      }
    }).then(res => res.json())
    .catch(error => {
      this.error = "error" + error;
    })
    .then(response => {
      if(response.mensaje){
        alert(response.mensaje)
      }
      else {
        this.movimientos = response.movimientos;
        this.saldo = response.saldo;
        this.nombre = response.nombres;
        localStorage.setItem("saldo", this.saldo)
        localStorage.setItem("nombre", this.nombre)
      }
    });
  }

  agregarMovimiento(){
    var url = 'https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/Movimientos/'+this.numeroCuenta;
    var _movimientos = this.movimientos;
    _movimientos.push({numeroCuentaDestino: this.numeroCuentaDestino, tipoMovimiento: this.tipoMovimiento, cantidad: this.cantidad, fecha: this.fecha});
    if(this.tipoMovimiento == "cargo"){
      this.saldo = this.saldo - this.cantidad;
    }else{
      this.saldo = parseFloat(this.saldo) + parseFloat(this.cantidad);
    }
    var data = {saldo: this.saldo, movimientos: _movimientos}
    fetch(url, {
      method: 'PUT', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json',
        'access-token': this.token
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      this.vistaDeAgregar = !this.vistaDeAgregar;
      this.getMovimientos();
    });
  }

  convertirDivisa(){
    if(this.moneda == "Dolares"){
      fetch("https://api.exchangeratesapi.io/latest?base=USD&symbols=MXN")
          .then(respuesta => respuesta.json())
          .then(respuestaDecodificada => {
            this.valorDolar = respuestaDecodificada.rates.MXN;
            this.fechaDolar = respuestaDecodificada.date;
            this.dividendo = this.valorDolar;
            this.moneda = "Pesos";
          });
    }else{
      this.dividendo = 1;
      this.moneda = "Dolares";
    }

  }

  cerrarSesion(e){
    localStorage.removeItem('token');
    localStorage.removeItem('numeroCuenta');
    this.logeado = !this.logeado;
  }

  cancelarMovimiento(){
    this.vistaDeAgregar = !this.vistaDeAgregar;
  }
}

customElements.define('component-principal', ComponentPrincipal);
